package com.sipios.refactoring.controller;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/shopping")
public class ShoppingController {

    private Logger logger = LoggerFactory.getLogger(ShoppingController.class);

    @PostMapping
    public String getPrice(@RequestBody Body body) {
        // price is 0 if there's no item in the cart
        if (body.getItems() == null || body.getItems().length == 0) {
            return "0";
        }

        // Compute discount for customer
        String customerType = body.getType();
        // get the discount based on the customer
        double discount = getDiscount(customerType);

        // Compute total amount depending on the types and quantity of product and
        // if we are in winter or summer discounts periods
        ItemsDiscount itemsDiscount;
        Date date = new Date();
        if (isSummerOrWinterDiscountPeriod(date)) {
            double dressDiscount = 0.8 * discount;
            double jacketDiscount = 0.9 * discount;
            itemsDiscount = new ItemsDiscount(discount, dressDiscount, jacketDiscount);
        } else {
            itemsDiscount = new ItemsDiscount(discount);
        }
        List<Item> items = Arrays.asList(body.getItems());
        double price = calculateTotalPrice(items, itemsDiscount);

        checkPriceFloor(customerType, price);

        return String.valueOf(price);
    }

    /**
     * Gets the discount given the customer type
     * @param customerType the customer type which discount is based on
     * @return the discount given the customer type
     * @throws ResponseStatusException if the customer type is unknown
     */
    private double getDiscount(String customerType) throws ResponseStatusException {
        double discount;
        switch (customerType) {
            case "STANDARD_CUSTOMER" :
                discount = 1;
                break;
            case "PREMIUM_CUSTOMER" :
                discount = 0.9;
                break;
            case "PLATINUM_CUSTOMER" :
                discount = 0.5;
                break;
            default:
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return discount;
    }

    /**
     * Calculate the total price of the cart, given the different item types, their quantity and their discounts
     * @param items items which total price should be calculated
     * @param itemsDiscount discounts on items given their type
     * @return the total price of the cart
     */
    private double calculateTotalPrice(List<Item> items, ItemsDiscount itemsDiscount) {
        double price = 0;
        for (Item item : items) {
            switch (item.getType()) {
                case "TSHIRT" :
                    price += 30 * item.getNb() * itemsDiscount.tshirtDiscount;
                    break;
                case "DRESS" :
                    price += 50 * item.getNb() * itemsDiscount.dressDiscount;
                    break;
                case "JACKET" :
                    price += 100 * item.getNb() * itemsDiscount.jacketDiscount;
                    break;
            }
        }

        return price;
    }

    /**
     * Returns true if the specified date is in summer or winter discounts periods
     * @param date the date to check
     * @return true if the specified date is in summer or winter discounts periods
     */
    private boolean isSummerOrWinterDiscountPeriod(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
        cal.setTime(date);

        if (
            (
                cal.get(Calendar.DAY_OF_MONTH) < 15 &&
                cal.get(Calendar.DAY_OF_MONTH) > 5 &&
                cal.get(Calendar.MONTH) == Calendar.JUNE
            ) ||
            (
                cal.get(Calendar.DAY_OF_MONTH) < 15 &&
                cal.get(Calendar.DAY_OF_MONTH) > 5 &&
                cal.get(Calendar.MONTH) == Calendar.JANUARY
            )
        ) {
            return true;
        }
        return false;
    }

    /**
     * Checks the price floor. Should throw an exception if the price is too high given the customer type
     * @param customerType the customer type
     * @param price price of items in the cart
     * @throws ResponseStatusException when the price is too high given the customer type
     */
    private void checkPriceFloor(String customerType, double price) throws ResponseStatusException {
        switch (customerType) {
            case "PREMIUM_CUSTOMER" :
                if (price > 800) {
                    String message = "Price (" + price + ") is too high for premium customer";
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
                }
                break;
            case "PLATINUM_CUSTOMER" :
                if (price > 2000) {
                    String message = "Price (" + price + ") is too high for platinum customer";
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
                }
                break;
            default:
                if (price > 200) {
                    String message = "Price (" + price + ") is too high for standard customer";
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
                }
                break;
        }
    }

    private class ItemsDiscount {
        double tshirtDiscount;
        double dressDiscount;
        double jacketDiscount;

        ItemsDiscount(double discount) {
            tshirtDiscount = discount;
            dressDiscount = discount;
            jacketDiscount = discount;
        }

        ItemsDiscount(double tshirtDiscount, double dressDiscount, double jacketDiscount) {
            this.tshirtDiscount = tshirtDiscount;
            this.dressDiscount = dressDiscount;
            this.jacketDiscount = jacketDiscount;
        }
    }
}

class Body {

    private Item[] items;
    private String type;

    public Body(Item[] is, String t) {
        this.items = is;
        this.type = t;
    }

    public Body() {}

    public Item[] getItems() {
        return items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

class Item {

    private String type;
    private int nb;

    public Item() {}

    public Item(String type, int quantity) {
        this.type = type;
        this.nb = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNb() {
        return nb;
    }

    public void setNb(int nb) {
        this.nb = nb;
    }
}
